package modelo;

import java.io.Serializable;

public class Eventos implements Serializable {
    private int id;
    private String nomeEvento;
    private String localEvento;
    private String dataEvento;

    public Eventos(int id, String nomeEvento, String localEvento, String dataEvento) {
        this.id = id;
        this.nomeEvento = nomeEvento;
        this.localEvento = localEvento;
        this.dataEvento = dataEvento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomeEvento() {
        return nomeEvento;
    }

    public void setNomeEvento(String nomeEvento) {
        this.nomeEvento = nomeEvento;
    }

    public String getLocalEvento() {
        return localEvento;
    }

    public void setLocalEvento(String localEvento) {
        this.localEvento = localEvento;
    }

    public String getDataEvento() {
        return dataEvento;
    }

    public void setDataEvento(String dataEvento) {
        this.dataEvento = dataEvento;
    }

    public String toString() {
        return nomeEvento + " // DATA: " + dataEvento + "// LOCAL: " + localEvento;
    }
}
