package com.example.list;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import modelo.Eventos;

public class MainActivity extends AppCompatActivity {

    private final int REQUEST_CODE_NOVO_EVENTO = 1;
    private final int REQUEST_CODE_EDITAR_EVENTO = 2;
    private final int RESULT_CODE_NOVO_EVENTO = 10;
    private final int RESULT_CODE_EVENTO_EDITADO = 11;

    private ArrayAdapter<Eventos> adaptereventos;
    private ListView lista_eventos;
    private int id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Eventos");

        lista_eventos = findViewById(R.id.lista_produtos);
        ArrayList<Eventos> eventos = new ArrayList<Eventos>();

        adaptereventos = new ArrayAdapter<Eventos>(MainActivity.this,
                android.R.layout.simple_list_item_1,
                eventos);
        lista_eventos.setAdapter(adaptereventos);



        onClickItemList();
        onLongClickItemListener();
    }

    private void onClickItemList(){
        lista_eventos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Eventos eventoClicado = adaptereventos.getItem(position);
                Intent intent = new Intent(MainActivity.this, cadastro.class);
                intent.putExtra("eventoEdicao", eventoClicado);
                startActivityForResult(intent, REQUEST_CODE_EDITAR_EVENTO);
            }
        });
    }

    private void onLongClickItemListener(){
        lista_eventos.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final Eventos eventoClicado = adaptereventos.getItem(position);
                new AlertDialog.Builder(MainActivity.this)
                        .setIcon((android.R.drawable.ic_delete))
                        .setTitle("Deseja realmente excluir o evento?")
                        .setMessage("deseja excluir este item ?")
                        .setPositiveButton("sim", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                adaptereventos.remove(eventoClicado);
                                adaptereventos.notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton("no", null).show();
                return false;
            }
        });
    }

    public void onClickNovoProduto(View v){
        Intent intent = new Intent(MainActivity.this, cadastro.class);
        startActivityForResult(intent, REQUEST_CODE_NOVO_EVENTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE_NOVO_EVENTO && resultCode == RESULT_CODE_NOVO_EVENTO){
            Eventos eventos = (Eventos) data.getExtras().getSerializable("novoProduto");
            eventos.setId(++id);
            this.adaptereventos.add(eventos);
        }
        if (requestCode == REQUEST_CODE_EDITAR_EVENTO && resultCode == RESULT_CODE_EVENTO_EDITADO){
            Eventos eventoEditado = (Eventos) data.getExtras().getSerializable("produtoEditado");
            for (int i = 0; i < adaptereventos.getCount(); i++){
                Eventos evento = adaptereventos.getItem(i);
                if (evento.getId() == eventoEditado.getId()){
                    adaptereventos.remove(evento);
                    adaptereventos.insert(eventoEditado, i);
                    break;
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}