package com.example.list;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import modelo.Eventos;

public class cadastro extends AppCompatActivity {
    private final int RESULT_CODE_NOVO_EVENTO = 10;
    private final int RESULT_CODE_EVENTO_EDITADO = 11;

    private boolean edicao = false;
    private int id = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        setTitle("Cadastro evento");
        carregarEvento();
    }

    private void carregarEvento(){
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null && intent.getExtras().get("eventoEdicao") != null){
            Eventos evento = (Eventos) intent.getExtras().get("eventoEdicao");
            EditText etnome = findViewById(R.id.ET_nome);
            EditText etlocal = findViewById(R.id.ET_valor);
            EditText etdata = findViewById(R.id.ET_data);
            etnome.setText(evento.getNomeEvento());
            etlocal.setText(evento.getLocalEvento());
            etdata.setText(evento.getDataEvento());
            edicao = true;
            id = evento.getId();
        }
    }

    public void onClickVoltar(View v){
        finish();
    }

    public void onClicSalvar(View v){
        EditText etnome = findViewById(R.id.ET_nome);
        EditText etlocal = findViewById(R.id.ET_valor);
        EditText etdata = findViewById(R.id.ET_data);

        String nome = etnome.getText().toString();
        String data = String.valueOf(etdata.getText());
        String local = etlocal.getText().toString();
        if (etnome.getText().length() != 0 && etdata.getText().length() != 0 && etlocal.getText().length() != 0) {
            Eventos evento = new Eventos(id, nome, local, data);
            Intent intent = new Intent();

            if (edicao) {
                intent.putExtra("produtoEditado", evento);
                setResult(RESULT_CODE_EVENTO_EDITADO, intent);
            } else {
                intent.putExtra("novoProduto", evento);
                setResult(RESULT_CODE_NOVO_EVENTO, intent);
            }
            finish();
        }else{
            AlertDialog alerta;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Campo nulo");
            builder.setMessage("Nenhum campo pode estar nulo");
            builder.setPositiveButton("Entendi", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                }
            });
            alerta = builder.create();
            alerta.show();
        }
    }
}